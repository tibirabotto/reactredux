import React from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as CourseActions from '../../store/actions/course'

const Sidebar = ({ modules, toogleLesson }) => (
    <aside>
        { modules.map(module => (
            <div key={module.id}>
                <strong>{module.title}</strong>
                <ul>
                    { module.lessons.map(lesson => (
                        <li key={lesson.id}>
                            { lesson.title }
                            <button onClick={() => 
                                toogleLesson(module, lesson)}>Select</button>
                        </li>
                    )) }
                </ul>
            </div>
        ))}
    </aside>
)

const mapStateToProps = state => ({
    modules: state.course.modules
})

const mapDispatchToPropos = dispatch => 
    bindActionCreators(CourseActions, dispatch)

export default connect(
    mapStateToProps, 
    mapDispatchToPropos
)(Sidebar)
