const INITIAL_STATE = {
    activeLesson: {},
    activeModule: {},
    modules: [{ 
        id: 1, 
        title: 'Starting with React', 
        lessons: [
            { id: 1, title: '1 lessons'},
            { id: 2, title: '2 lessons'},
        ]
    },
    { 
        id: 2, 
        title: 'Learning Redux', 
        lessons: [
            { id: 3, title: '3 lessons'},
            { id: 4, title: '4 lessons'}
        ]
    }]
}

export default function course(state = INITIAL_STATE, action){
    if (action.type === 'TOOGLE_LESSON') {
        return { 
            ...state,  
            activeLesson: action.lesson,
            activeModule: action.module
        }
    }
    return state
}